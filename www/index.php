<?php
use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use Facebook\Entities\AccessToken;
use Nette\Utils\Strings;
use Nette\Caching\Cache;
use Tracy\Debugger;

//composer require facebook/php-sdk-v4 tracy/tracy latte/latte nette/utils nette/caching nette/neon
require '../vendor/autoload.php';

//Load configuration
$config = Nette\Neon\Neon::decode(file_get_contents('../config.neon'));

//Create all dirs we will need
$dirs = array($config['cache']['dir'],
	$config['tracy']['logDir'],
	$config['template']['dir'],
	$config['template']['cacheDir']
);

foreach ($dirs as $dir) {
	if (!is_dir($dir)) {
		mkdir($dir, 0777, TRUE);
	}
}

//Load and config Tracy
Debugger::enable(Debugger::DETECT, __DIR__ . '/' . $config['tracy']['logDir']);
Debugger::$email = $config['tracy']['logMail'];

//Load and config caching
$storage = new Nette\Caching\Storages\FileStorage($config['cache']['dir']);
$cache = new Cache($storage);

if (empty($cache->load('isValid'))) {
	//Set default facebook app
	FacebookSession::setDefaultApplication($config['facebook']['appId'], $config['facebook']['appSecret']);

	//Set a new session using keys from config
	$session = FacebookSession::newAppSession($config['facebook']['appId'], $config['facebook']['appSecret']);

	//Create a request for Graph API
	$request = new FacebookRequest(
		$session,
		'GET',
		'/'.$config['facebook']['groupId'].'/feed'
	);

	try {
		//Execute request and get desired data from it
		$posts = $request->execute()->getGraphObject()->asArray()['data'];

		//Cache response so we don't have to query facebook every time.
		$cache->save($config['facebook']['groupId'], $posts);

		$cache->save('isValid', true, [
			Cache::EXPIRE => $config['cache']['expiry'],
		]);

	} catch (Exception $e) { //We catch all exceptions but only care about FB ones, the rest is thrown again.

		if (   $e instanceof Facebook\FacebookSDKException
			|| $e instanceof Facebook\FacebookRequestException
			|| $e instanceof Facebook\FacebookServerException
		) {
			//If we can't get posts from facebook, we'll keep on using cache and try next time.
			$posts = $cache->load($config['facebook']['groupId']);

			$cache->save('isValid', 'needsRenewal', [
				Cache::EXPIRE => $config['cache']['expiry'],
			]);

		} else {
			throw $e;
		}
	}
} else {
	$posts = $cache->load($config['facebook']['groupId']);
}

//Initialize Latte templating engine
$latte = new Latte\Engine;

//Set directory for template caching
$latte->setTempDirectory($config['template']['cacheDir']);

//Add latte filter for custom DateTime conversion
$latte->addFilter('myDate', function ($input, $lang, $format = 'j. n. Y', $timezone) {
	$date = new DateTime($input);
	$now = new DateTime;

	$date->setTimezone(new DateTimeZone($timezone));
	$now->setTimezone(new DateTimeZone($timezone));

	$interval = $now->getTimestamp() - $date->getTimestamp();

	if ($interval <= 1) {
		return $lang['now'];

	} elseif ($interval < 60) {
		return sprintf($lang['n_seconds_ago'], $date->diff($now)->format('%s'));

	} elseif ($interval < 2*60) {
		return $lang['minute_ago'];

	} elseif ($interval < 60*60) {
		return sprintf($lang['n_minutes_ago'], $date->diff($now)->format('%i'));

	} elseif ($interval < 2*60*60) {
		return $lang['hour_ago'];

	} elseif ($date->getTimestamp() >= (new DateTime())->setTime(0,0)->getTimestamp()) {
		return sprintf($lang['n_hours_ago'], $date->diff($now)->format('%h'));

	} elseif ($date->getTimestamp() >= (new DateTime())->setTime(0,0)->modify('-1 day')->getTimestamp()) {
		return $lang['yesterday'];

	} elseif ($date->getTimestamp() >= (new DateTime())->setTime(0,0)->modify('-6 day')->getTimestamp()) {
		$days = $date->diff($now)->format('%d');
		if ($date->format('Gis') > $now->format('Gis')) {
			$days++;
		}
		return sprintf($lang['n_days_ago'], $days);
	}
	return $date->format($format);
});

//Create list of variables that will be passed to template
$params = array(
	'posts' => $posts,
	'config' => $config['template'],
	'lang' => $config['language'],
	'facebook' => $config['facebook'],
	'appConfig' => $config['app'],
	'connectionWarning' => ($cache->load('isValid') === 'needsRenewal') ? TRUE : FALSE
);

//Render the template
$latte->render($config['template']['dir'].'index.latte', $params);