Facebook news widget for bronies.cz

To use this, you need to put facebook app id and secret key to config file (config.neon). You can get it [here](https://developers.facebook.com/apps/).

### External libraries ###
* facebook/php-sdk-v4
* tracy/tracy
* latte/latte
* nette/utils
* nette/caching
* nette/neon